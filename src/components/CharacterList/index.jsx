import { memo } from 'react'

import CharacterCard from '../CharacterCard'
import EmptyData from '../Global/EmptyData'

import StyledList from './StyledList'
import StyledListItem from './StyledListItem'

const CharacterList = ({ characterDefaultList }) => {
  const list =
    !characterDefaultList.length > 0 ? (
      <EmptyData>No characters is found</EmptyData>
    ) : (
      <StyledList>
        {characterDefaultList.map((char) => (
          <StyledListItem key={char.id}>
            <CharacterCard character={char} />
          </StyledListItem>
        ))}
      </StyledList>
    )

  return <>{list}</>
}

export default memo(CharacterList)
