import styled from 'styled-components'

const StyledListItem = styled.li`
  border-radius: var(--border-rad);
  border: 1px solid var(--border-color-primary);
  margin: 2px;
`

export default StyledListItem
