import styled from 'styled-components'

const StyledList = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0.3rem 0;
  padding-left: 0;

  @media (min-width: 650px) {
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
  }

  @media (min-width: 1024px) {
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: start;
    padding-left: 1rem;
  }
`
export default StyledList
