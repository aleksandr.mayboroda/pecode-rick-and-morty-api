import styled from 'styled-components'

const LeftSideBlock = styled.div`
  display: flex;
  flex-direction: column;
  padding-right: 1rem;
  padding-left: 1rem;
`

export default LeftSideBlock