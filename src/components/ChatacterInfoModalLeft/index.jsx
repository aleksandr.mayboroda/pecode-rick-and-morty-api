import { memo } from 'react'
import dayjs from 'dayjs'

import LeftSideBlock from './LeftSideBlock'
import CharacterInfoRow from './CharacterInfoRow'

const ChatacterInfoModalLeft = ({
  created,
  gender,
  species,
  status,
  location,
  origin,
  type,
}) => {
  const birthDate = dayjs(created).format('DD MMM YYYY')

  const renderType = !!type ? (
    <CharacterInfoRow text="type" value={type} />
  ) : null

  return (
    <LeftSideBlock>
      <CharacterInfoRow text="birth date" value={birthDate} />
      <CharacterInfoRow text="gender" value={gender} />
      <CharacterInfoRow text="species" value={species} />
      <CharacterInfoRow text="status" value={status} />
      <CharacterInfoRow text="origin" value={origin.name} />
      {renderType}
      <CharacterInfoRow text="location" value={location.name} />
    </LeftSideBlock>
  )
}

export default memo(ChatacterInfoModalLeft)
