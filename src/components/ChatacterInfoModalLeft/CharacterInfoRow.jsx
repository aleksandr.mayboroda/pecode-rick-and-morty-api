import React from 'react'

import InfoRow from '../Global/InfoRow'
import InfoTitle from '../Global/InfoTitle'
import InfoText from '../Global/InfoText'

const CharacterInfoRow = ({ text, value }) => {
  return (
    <InfoRow>
      <InfoTitle>{text}:</InfoTitle>
      <InfoText>{value}</InfoText>
    </InfoRow>
  )
}

export default CharacterInfoRow
