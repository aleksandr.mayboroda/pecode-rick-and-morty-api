import { memo, useCallback } from 'react'

import Wrapper from './ControlsWrapper'
import StyledInput from '../Form/StyledInput'
import Error from '../Global/Error'
import StyledSelect from '../Form/StyledSelect'

const statusObject = [
  { name: 'Select status', value: '' },
  { name: 'Alive', value: 'alive' },
  { name: 'Dead', value: 'dead' },
  { name: 'Unknown', value: 'unknown' },
]

const genderObject = [
  { name: 'Select gender', value: '' },
  { name: 'Female', value: 'female' },
  { name: 'Male', value: 'male' },
  { name: 'Genderless', value: 'genderless' },
  { name: 'Unknown', value: 'unknown' },
]

const Controls = ({ filters, filterChange, isError }) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoFilterChange = useCallback((ev) => filterChange(ev.target.name, ev.target.value),[])
  return (
    <>
      <Wrapper>
        <StyledInput
          type="text"
          placeholder="type spacies"
          name="species"
          value={filters.species}
          onChange={memoFilterChange}
        />
         <StyledSelect
          list={genderObject}
          name="gender"
          value={filters.gender}
          onChange={memoFilterChange}
        />
        <StyledSelect
          list={statusObject}
          name="status"
          value={filters.status}
          onChange={memoFilterChange}
        />
      </Wrapper>
      {isError && <Error>{isError}</Error>}
    </>
  )
}

export default memo(Controls)
