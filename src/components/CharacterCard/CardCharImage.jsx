import styled from 'styled-components'

const CardCharImage = styled.img`
  max-width: 100%;
  object-fit: cover;
`
export default CardCharImage