import { memo, useCallback, useMemo } from 'react'
// import { getOneCharacter } from '../../api/character'

import useModal from '../../hooks/useModal'

import ChatacterInfoModal from '../ChatacterInfoModal'

import CardWrapper from './CardWrapper'
import CardCharName from './CardCharName'
import CardCharImageBlock from './CardCharImageBlock'
import CardCharImage from './CardCharImage'
import CardNotLoadedChar from './CardNotLoadedChar'

const CharacterCard = ({ character }) => {
  const { name, image } = character

  const memoCharacter = useMemo(() => character, [character])

  const { modalOpen } = useModal()

  const handleModal = useCallback(
    () =>
      Object.keys(character).length > 0
        ? modalOpen(<ChatacterInfoModal character={memoCharacter} />)
        : modalOpen(
            <CardNotLoadedChar>
              Somethin' with server, try later
            </CardNotLoadedChar>
          ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [character]
  )

  return (
    // <CardWrapper onClick={loadCharInfo}>
    <CardWrapper onClick={handleModal}>
      <CardCharName>{name}</CardCharName>
      <CardCharImageBlock>
        <CardCharImage src={image} alt={name} />
      </CardCharImageBlock>
    </CardWrapper>
  )
}

export default memo(CharacterCard)
