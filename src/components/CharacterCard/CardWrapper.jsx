import styled from 'styled-components'

const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
export default CardWrapper