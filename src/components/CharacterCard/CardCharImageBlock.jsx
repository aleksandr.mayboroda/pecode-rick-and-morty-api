import styled from 'styled-components'

const CardCharImageBlock = styled.div`
  width: 80%;
  margin: auto;
`

export default CardCharImageBlock