import styled from 'styled-components'

const CardNotLoadedChar = styled.h2`
  min-height: 200px;
  text-align: center;
  line-height: 200px;
  font-size: 2rem;
`

export default CardNotLoadedChar