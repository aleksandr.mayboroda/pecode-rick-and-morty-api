import styled from 'styled-components'

const CardCharName = styled.h2`
  text-align: center;
  background-color: $bg-primary;
  max-width: 240px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

`

export default CardCharName