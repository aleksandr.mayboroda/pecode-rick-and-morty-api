import { useEffect, useCallback, memo } from 'react'

//apollo
import { useLazyQuery, useReactiveVar } from '@apollo/client'
import { GET_CHARACTERS } from '../../apollo/operations/query/getCharacters'
import {
  charListPage,
  setCharListPage,
  charFilters,
  setCharFilter,
} from '../../apollo/vars/chatacter'

import useDebounce from '../../hooks/useDebounce'

import Controls from '../Controls'
import CharacterList from '../CharacterList'
import Pagination from '../Pagination'
import Preloader from '../Preloader'

const Characters = () => {
  const page = useReactiveVar(charListPage)
  const filter = useReactiveVar(charFilters)

  //apollo
  const [loadCharacters, { loading, error: charListError, data }] =
    useLazyQuery(GET_CHARACTERS, {
      variables: {
        page,
        filter,
      },
    })
  // console.log(123, charListLoading, data, charListError)

  const setPage = useCallback(
    setCharListPage,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [page]
  )

  const filterChange = useCallback(
    setCharFilter,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const loadCharacterDebounced = useDebounce(loadCharacters, 500)

  //watch page change
  useEffect(() => {
    loadCharacterDebounced()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  //watch filters change
  useEffect(() => {
    const { species, gender, status } = filter
    if (species || gender || status) {
      setPage(1)
    }
    loadCharacterDebounced()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  const list =
    !data?.characters?.results.length > 0 ? null : (
      <CharacterList characterDefaultList={data?.characters?.results} />
    )

  const pagination =
    !data?.characters?.results.length > 0 ? null : (
      <Pagination
        {...data?.characters?.info}
        activePage={page}
        pageChange={setPage}
      />
    )

  return (
    <>
      <Controls
        filterChange={filterChange}
        filters={filter}
        isError={charListError}
      />
      {loading && <Preloader loading={loading} />}
      {pagination}
      {list}
      {pagination}
    </>
  )
}

export default memo(Characters)
