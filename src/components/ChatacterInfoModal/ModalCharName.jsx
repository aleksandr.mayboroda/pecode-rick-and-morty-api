import styled from 'styled-components'

const ModalCharName = styled.h2`
text-align: center;
font-size: 2rem;
`

export default ModalCharName