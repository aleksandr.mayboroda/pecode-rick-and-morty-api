import styled from 'styled-components'

const ModalTwoSidesBlock = styled.div`
  margin-bottom: 1rem;
  display: grid;
  grid-template-columns: repeat(1, 1fr);

  @media (min-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }
`

export default ModalTwoSidesBlock