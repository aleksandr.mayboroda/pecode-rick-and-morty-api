import { memo } from 'react'

import ChatacterInfoModalEpisodeList from '../ChatacterInfoModalEpisodeList'
import ChatacterInfoModalLeft from '../ChatacterInfoModalLeft'

import InfoTitle from '../Global/InfoTitle'
import InfoRowBase from '../Global/InfoRowBase'

import ModalCharName from './ModalCharName'
import ModalTwoSidesBlock from './ModalTwoSidesBlock'
import ModalRightSideBlock from './ModalRightSideBlock'

const ChatacterInfoModal = ({ character }) => {
  const { name, image, episode } = character

  return (
    <div>
      <ModalCharName>{name}</ModalCharName>
      <ModalTwoSidesBlock>
        <ChatacterInfoModalLeft {...character} />
        <ModalRightSideBlock>
          <img src={image} alt={name} />
        </ModalRightSideBlock>
      </ModalTwoSidesBlock>
      <div>
        <InfoRowBase>
          <InfoTitle>episodes:</InfoTitle>
          <ChatacterInfoModalEpisodeList list={episode} />
        </InfoRowBase>
      </div>
    </div>
  )
}

export default memo(ChatacterInfoModal)
