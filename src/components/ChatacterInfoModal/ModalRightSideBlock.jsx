import styled from 'styled-components'

const ModalRightSideBlock = styled.div`
  display: flex;
  justify-content: center;
  order: -1;
  margin-bottom: 0.5rem;

  & > img {
    max-width: 100%;
    object-fit: content;
  }

  @media (min-width: 768px) {
    order: inherit;
    margin-bottom: 0;
  }

  @media (min-width: 1024px) {
    & > img {
      max-width: 244px;
      max-height: 244px;
    }
  }
`

export default ModalRightSideBlock