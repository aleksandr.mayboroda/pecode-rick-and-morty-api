import {memo} from 'react'

import SearchList from '../Global/SearchList'
import EmptyData from '../Global/EmptyData'
import EpisodeSearchListItem from '../EpisodeSearchListItem'

const EpisodesList = ({ episodeList }) => {
  const foundEpisodes =
    !episodeList.length > 0 ? (
      <EmptyData>No episode is found</EmptyData>
    ) : (
      <SearchList>
        {episodeList.map((props) => (
          <EpisodeSearchListItem key={props.id} {...props} />
        ))}
      </SearchList>
    )

  return <>{foundEpisodes}</>
}

export default memo(EpisodesList)
