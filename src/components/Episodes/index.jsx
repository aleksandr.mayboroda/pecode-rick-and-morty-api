import React from 'react'

import Wrapper from './Wrapper'
import EpisodesToSearch from '../EpisodesToSearch'
import EpisodesToWatch from '../EpisodesToWatch'


const Episodes = () => {
  return (
    <Wrapper>
      <EpisodesToSearch />
      <EpisodesToWatch />
    </Wrapper>
  )
}

export default Episodes
