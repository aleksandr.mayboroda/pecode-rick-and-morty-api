import styled from 'styled-components'

const Wrapper = styled.div`
display: grid;
grid-template-columns: (1, 1fr);
gap: 0.5rem;

@media (min-width: 768px) {
  grid-template-columns: repeat(2, 1fr);
}
`

export default Wrapper