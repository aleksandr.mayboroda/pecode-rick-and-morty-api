import styled from 'styled-components'

import image from './boundary.jpg'

const Block = styled.div`
  background: url(${image}) no-repeat center;
  background-clip: border-box;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  padding-left: 1rem;
  align-items: center;
`

export default Block