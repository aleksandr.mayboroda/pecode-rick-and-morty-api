import styled from 'styled-components'

const Text = styled.h1`
  color: #fff;
  font-size: 30px;
  text-transform: uppercase;
  letter-spacing: 2px;
  animation: runningText 3s linear both;
  animation-delay: 0;

  @keyframes runningText {
    0% {
      margin-left: -500px;
    }
    50% {
      margin-left: -250px;
    }
    100% {
      margin-left: 50px;
    }
  }
`

export default Text