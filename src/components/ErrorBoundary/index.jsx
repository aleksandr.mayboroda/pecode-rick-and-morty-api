import React, { PureComponent } from 'react'

import Container from '../Global/Container'
import Block from './Block'
import Text from './Text'

class ErrorBoundary extends PureComponent {
  state = {
    isError: null,
  }

  static getDerivedStateFromError(error) {
    return { isError: error }
  }

  render() {
    if (this.state.isError) {
      return (
        <Container>
          <Block>
            <Text>Something went wrong...</Text>
          </Block>
        </Container>
      )
    }
    return this.props.children
  }
}

export default ErrorBoundary
