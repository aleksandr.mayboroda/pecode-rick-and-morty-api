import React from 'react'
import { Backdrop } from '@mui/material'

const Preloader = ({ loading }) => {
  return <Backdrop open={loading} />
}

export default Preloader
