import styled from 'styled-components'

const ButtonWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`

export default ButtonWrapper