import styled from 'styled-components'

const StyledButton = styled.button`
  min-width: 43px;
  text-align: center;
  transition: 0.3s;
  font-weight: bolder;
  border: 1px solid var(--border-color-primary);
  border-radius: var(--border-rad);
  margin: 0.2rem;
  padding: 0.7rem;
  cursor: pointer;
  background-color: ${(props) =>
    props.background ? props.background : 'var(--bg-btn-primary)'};
  color: ${(props) => (props.color ? props.color : 'var(--color-btn-primary)')};
  border-color: ${(props) =>
    props.borderColor ? props.borderColor : 'var(--border-color-primary)'};
`

export default StyledButton
