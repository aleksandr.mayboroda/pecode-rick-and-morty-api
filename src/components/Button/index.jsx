import React from 'react'

import ButtonWrapper from './ButtonWrapper'
import StyledButton from './StyledButton'

const Button = ({
  text = 'NO Text',
  onClick,
  background,
  color,
  borderColor,
}) => {
  return (
    <ButtonWrapper>
      <StyledButton
        onClick={onClick}
        background={background}
        color={color}
        borderColor={borderColor}
      >
        {text}
      </StyledButton>
    </ButtonWrapper>
  )
}

export default Button
