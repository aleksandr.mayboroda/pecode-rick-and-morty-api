import styled from 'styled-components'

const ModalClose = styled.span`
  position: absolute;
  right: 15px;
  top: 15px;
  width: 21px;
  height: 21px;
  opacity: 0.3;
  content: '';
  transition: 0.5s;

  color: var(--color-text-secondary);

  &:hover {
    opacity: 1;
  }
  &::before,
  &::after {
    position: absolute;
    left: 10px;
    content: ' ';
    height: 20px;
    width: 2px;
    background-color: var(--bg-secondary);
  }
  &::before {
    transform: rotate(45deg);
  }
  &::after {
    transform: rotate(-45deg);
  }
`

export default ModalClose
