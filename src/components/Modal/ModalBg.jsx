import styled from 'styled-components'

const ModalBg = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  top: 0;
  left: 0;
  display: ${({isOpen}) => isOpen ? 'flex' : 'none'};
  opacity: ${({isOpen}) => isOpen ? '1' : '0'};
  pointer-events: ${({isOpen}) => isOpen ? 'all' : 'none'};
  justify-content: center;
  align-items: center;
  transition: 0.5s;
`

export default ModalBg