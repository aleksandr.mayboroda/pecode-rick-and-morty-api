import React from 'react'
import { useReactiveVar } from '@apollo/client'
import { modalContent } from '../../apollo/vars/modal'
import useModal from '../../hooks/useModal'

import ModalBg from './ModalBg'
import ModalWindow from './ModalWindow'
import ModalClose from './ModalClose'

const Modal = () => {
  const content = useReactiveVar(modalContent)
  const { modalClose } = useModal()
  const isOpen = !!content

  return (
    <ModalBg isOpen={isOpen} onClick={modalClose}>
      <ModalWindow isOpen={isOpen}>
        <ModalClose onClick={modalClose} />
        {content}
      </ModalWindow>
    </ModalBg>
  )
}

export default Modal
