import styled from 'styled-components'

const ModalWindow = styled.div`
  padding: 0.5rem;
  border-radius: var(--border-rad);
  background-color: var(--bg-third);
  color: var(--color-text-secondary);
  position: relative;
  width: 90vw;
  transform: ${({ isOpen }) => (isOpen ? 'scale(1)' : 'scale(0.5)')};
  transition: 0.7s;

  @media (min-width: 1024px) {
    width: 60vw;
  }

  @media (min-width: 1368px) {
    width: 45vw;
  }
`

export default ModalWindow
