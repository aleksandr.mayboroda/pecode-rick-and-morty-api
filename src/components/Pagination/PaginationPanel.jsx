import styled from 'styled-components'

const PaginationPanel = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  padding-left: 1rem;
  ${(props) => props?.styles && props.styles}
`

export default PaginationPanel