import { useState, useEffect, useCallback, memo } from 'react'

import PaginationPanel from './PaginationPanel'
import PaginationArrow from './PaginationArrow'
import PaginationButton from './PaginationButton'

const Pagination = ({
  pages,
  pageChange = () => {},
  activePage,
  userSettings,
}) => {
  const [arrayOfCurrentButton, setArrayOfCurrentButton] = useState([])

  const defaultSettings = {
    dotsLeft: ' ...', //with space for difference in function
    dotsRight: '... ', //with space for difference in function
    arrows: {
      //arrows settinsg object
      prevText: 'prev',
      nextText: 'next',
    },
  }

  const settings = { ...defaultSettings, userSettings }

  //array with all pages
  const pageNumbers = []
  for (let i = 1; i <= pages; i++) {
    pageNumbers.push(i)
  }

  const btnClick = useCallback(
    (number) => {
      if (number === settings.dotsLeft) {
        number = activePage - 2
      }
      if (number === settings.dotsRight) {
        number = activePage + 2
      }
      pageChange(number)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [activePage]
  )

  useEffect(() => {
    const newPageNumbers = []

    if (activePage > 2) {
      newPageNumbers.push(1)
      if (activePage > 3) {
        newPageNumbers.push(settings.dotsLeft)
      }
    }
    let start = activePage - 1
    let end = activePage
    if (activePage > 1) {
      start = activePage - 2
    }
    if (activePage < pageNumbers.length) {
      end = activePage + 1
    }

    newPageNumbers.push(...pageNumbers.slice(start, end))

    if (activePage < pageNumbers.length - 1) {
      if (activePage < pageNumbers.length - 2) {
        newPageNumbers.push(settings.dotsRight)
      }
      newPageNumbers.push(pageNumbers.length)
    }

    setArrayOfCurrentButton(newPageNumbers)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activePage, pages])

  const buttons =
    !arrayOfCurrentButton.length > 0
      ? null
      : arrayOfCurrentButton.map((number, index) => (
          <PaginationButton
            key={index}
            className={number === activePage && 'active'}
            onClick={() => btnClick(number)}
          >
            {number}
          </PaginationButton>
        ))

  return (
    <PaginationPanel>
      <PaginationArrow
        disabled={activePage <= 1}
        onClick={() => pageChange(activePage > 1 ? --activePage : activePage)}
      >
        {settings.arrows.prevText}
      </PaginationArrow>
      {buttons}
      <PaginationArrow
        disabled={activePage === pages}
        onClick={() =>
          pageChange(activePage < pages ? ++activePage : activePage)
        }
      >
        {settings.arrows.nextText}
      </PaginationArrow>
    </PaginationPanel>
  )
}

export default memo(Pagination)
