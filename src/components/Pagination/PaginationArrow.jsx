import styled from 'styled-components'

const PaginationArrow = styled.button`
  outline: none;
  background-color: transparent;
  min-width: 43px;
  text-align: center;
  transition: 0.3s;
  font-weight: bolder;
  border: 1px solid var(--border-color-primary);
  border-radius: var(--border-rad);
  color: var(--color-text-primary);
  margin-rigth: 0.3rem;
  padding: 0.7rem;
  cursor: pointer;
  text-transform: capitalize;
  :disabled {
    background-color: var(--bg-secondary);
    pointer-events: none;
    opacity: 0.6;
    cursor: inherit;
  }

  ${(props) => props?.styles && props.styles}
`

export default PaginationArrow
