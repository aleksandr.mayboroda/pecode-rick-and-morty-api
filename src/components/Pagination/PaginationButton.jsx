import styled from 'styled-components'

const PaginationButton = styled.button`
  outline: none;
  background-color: transparent;
  min-width: 43px;
  text-align: center;
  transition: 0.3s;
  font-weight: bolder;
  border: 1px solid var(--border-color-primary);
  border-radius: var(--border-rad);
  color: var(--color-text-primary);
  margin: 0.2rem;
  padding: 0.7rem;
  cursor: pointer;

  :hover {
    border-color: var(--border-color-secondary);
    color: var(--color-text-primary);
  }

  &.active {
    color: var(--color-text-active);
    background-color: var(--bg-third);
  }

  ${(props) => props?.styles && props.styles}
`

export default PaginationButton