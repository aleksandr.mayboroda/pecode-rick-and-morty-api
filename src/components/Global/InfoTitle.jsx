import styled from 'styled-components'

const InfoTitle = styled.span`
  color: var(--color-text-third);
  font-size: 1.5rem;
  font-weight: bolder;
`

export default InfoTitle