import styled from 'styled-components'

const InfoRowBase = styled.div`
  display: flex;
  padding-left: 1rem;
`

export default InfoRowBase