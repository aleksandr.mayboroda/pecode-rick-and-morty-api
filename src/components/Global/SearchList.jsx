import styled from 'styled-components'

const SearchList = styled.ul`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  padding: 0;

  @media (min-width: 420px) {
    padding: 0 0.3rem;
  }
`
export default SearchList
