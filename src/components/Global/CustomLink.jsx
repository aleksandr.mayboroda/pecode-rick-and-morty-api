import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

const CustomLink = styled(NavLink)((props) => ({
  padding: '1rem',
  border: '1px solid var(--border-color-primary)',
  borderTopLeftRadius: 'var(--border-rad)',
  borderTopRightRadius: 'var(--border-rad)',
  textTransform: 'uppercase',
  outline: 'none',
  color: 'var(--color-text-third)',

  '&.active': {
    color: 'var(--color-text-primary)',
    borderBottom: 'none',
  },
}))

export default CustomLink