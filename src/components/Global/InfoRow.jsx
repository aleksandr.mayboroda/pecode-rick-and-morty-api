import styled from 'styled-components'

import InfoRowBase from './InfoRowBase'

const InfoRow = styled(InfoRowBase)`
  display: flex;
  justify-content: space-between;
  padding-left: 0;
`

export default InfoRow
