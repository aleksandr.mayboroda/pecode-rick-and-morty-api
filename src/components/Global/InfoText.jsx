import styled from 'styled-components'

const InfoText = styled.span`
  color: var(--color-text-secondary);
  font-size: 1.8rem;
  font-weight: bolder;
  padding-left: 0.5rem;
  text-align: right;
  text-transform: lowercase;
`
export default InfoText