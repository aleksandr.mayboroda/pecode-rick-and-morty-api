import styled from 'styled-components'

const Error = styled.h2`
  text-align: center;
  font-weight: bolder;
  color: var(--color-text-error);
`

export default Error