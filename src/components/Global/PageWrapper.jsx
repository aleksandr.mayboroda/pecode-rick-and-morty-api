import styled from 'styled-components'

const PageWrapper = styled('div')(() => ({
  padding: '1rem',
  backgroundColor: 'var(--bg-secondary)',
  flexGrow: 1,
}))


export default PageWrapper
