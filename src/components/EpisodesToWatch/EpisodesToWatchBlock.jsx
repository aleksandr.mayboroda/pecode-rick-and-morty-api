import styled from 'styled-components'

const EpisodesToWatchBlock = styled.div`
  order: -1;
  @media (min-width: 768px) {
    order: inherit;
  }
`

export default EpisodesToWatchBlock
