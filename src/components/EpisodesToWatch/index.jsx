import { memo } from 'react'

import EpisodesToWatchBlock from './EpisodesToWatchBlock'
import Title from './EpisodesBlockTitle'
import EpisodesToWatchList from '../EpisodesToWatchList'

const EpisodesToWatch = () => {
  return (
    <EpisodesToWatchBlock>
      <Title>Episodes to watch</Title>
      <EpisodesToWatchList />
    </EpisodesToWatchBlock>
  )
}

export default memo(EpisodesToWatch)
