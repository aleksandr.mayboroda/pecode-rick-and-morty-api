import { useEffect, useCallback, memo } from 'react'

import useDebounce from '../../hooks/useDebounce'

//apollo
import { useLazyQuery, useReactiveVar } from '@apollo/client'
import { GET_EPISODES } from '../../apollo/operations/query/getEpisodes'
import {
  episodeListPage,
  setEpisodeListPage,
  episodeFilters,
  setEpisodeFilter,
} from '../../apollo/vars/episode'

import StyledInput from '../Form/StyledInput'
import EpisodesList from '../EposodesList'
import Pagination from '../Pagination'
import Error from '../Global/Error'
import Preloader from '../Preloader'

const EpisodesToSearch = () => {
  const page = useReactiveVar(episodeListPage)
  const filter = useReactiveVar(episodeFilters)

  const setPage = useCallback(setEpisodeListPage, [page])
  const filterChange = useCallback(
    (ev) => setEpisodeFilter(ev.target.name, ev.target.value),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const [loadEpisodes, { loading, error, data }] = useLazyQuery(GET_EPISODES, {
    variables: {
      page,
      filter,
    },
  })

  const loadEpisodesDebaounced = useDebounce(loadEpisodes, 500)

  useEffect(() => {
    loadEpisodesDebaounced()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  useEffect(() => {
    setPage(1)
    loadEpisodesDebaounced()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter])

  const foundEpisodes =
    !data?.episodes?.results.length > 0 ? null : (
      <EpisodesList episodeList={data?.episodes?.results} />
    )

  const pagination =
    !data?.episodes?.results.length > 0 ? null : (
      <Pagination
        {...data?.episodes?.info}
        activePage={page}
        pageChange={setPage}
      />
    )

  return (
    <div>
      {loading && <Preloader loading={loading} />}
      <StyledInput
        type="text"
        placeholder="search episode"
        name="name"
        value={filter?.name}
        onChange={filterChange}
      />
      {error && <Error>{error}</Error>}
      {pagination}
      {foundEpisodes}
      {pagination}
    </div>
  )
}

export default memo(EpisodesToSearch)
