import React from 'react'
import { FormControl, Select, MenuItem, styled } from '@mui/material'

const StyledSelectElem = styled(Select)`
  & .list {
    background-color: var(--bg-primary);
    color: var(--color-text-primary);
  }

  & .MuiSelect-select {
    border-radius: var(--border-rad);
    font-weight: 400;
  }

  & .MuiOutlinedInput-input {
    outline: none;
    font-weight: 400;
    font-size: 16px;
    padding: 1rem;
    box-sizing: border-box;
    border-radius: var(--border-rad);
    border: 1px solid var(--border-color-primary);
    background-color: var(--bg-primary);
    color: var(--color-text-primary);
    line-height: normal;
    text-transform: lowercase;
    min-width: 120px;
  }

  // delete blue outline on focus
  &.Mui-focused .MuiOutlinedInput-notchedOutline {
    border: none;
  }

  // arrow color
  & > svg {
    color: var(--color-text-primary);
  }

`

const StyledSelect = ({ list, value, onChange, name }) => {

  const options = !list.length > 0 ? null : (
    list.map(({ name, value }) => (
      <MenuItem key={value} value={value.toLowerCase()}>
        {name.toLowerCase()}
      </MenuItem>
    ))
  )

  return (
    <FormControl
      sx={{
        padding: '1rem 2rem',
        display: 'flex',
        alignItems: 'center',
        borderRadius: 'var(--border-rad)',
        boxShadow: 'var(--shadow)',
        marginBottom: '1rem',
      }}
    >
      <StyledSelectElem
        displayEmpty
        name={name}
        value={value}
        onChange={onChange}
        MenuProps={{
          sx: {
            marginTop: '0.5rem',
            borderRadius: 'var(--border-rad)',
            '&& .MuiList-root': {
              backgroundColor: 'var(--bg-primary)',
              color: 'var(--color-text-primary)',
             
            },
            '&& .Mui-selected': {
              backgroundColor: 'var(--color-text-third)',
            },
          },
        }}
      >
        {options}
      </StyledSelectElem>
    </FormControl>
  )
}

export default StyledSelect
