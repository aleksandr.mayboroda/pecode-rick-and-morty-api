import { memo } from 'react'
import styled from 'styled-components'

const InputContainer = styled.label`
  padding: 1rem 2rem;
  display: flex;
  align-items: center;
  border-radius: var(--border-rad);
  box-shadow: var(--shadow);
  margin-bottom: 1rem;
`

const Input = styled.input`
  outline: none;
  background-color: var(--bg-primary);
  color: var(--color-text-primary);
  width: 100%;
  padding: 1rem;
  border-radius: var(--border-rad);
  border: 1px solid var(--border-color-primary);
  font-size: 16px;
`

const StyledInput = (props) => {
  return (
    <InputContainer>
      <Input {...props} />
    </InputContainer>
  )
}

export default memo(StyledInput)
