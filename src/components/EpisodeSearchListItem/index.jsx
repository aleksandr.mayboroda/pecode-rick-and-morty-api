import { memo, useCallback } from 'react'

import {
  toggleEpisodeInWatchList,
  toggleEpisodeWatchListStatus,
} from '../../apollo/vars/episode'

import useDebounce from '../../hooks/useDebounce'

import Button from '../Button'
import SearchListItem from './SearchListItem'
import ButtonBlock from './ButtonBlock'
import StyledFormControlLabel from './StyledFormControlLabel'
import StyledCheckbox from './StyledCheckbox'

const EpisodeSearchListItem = ({
  id,
  name,
  episode,
  isInWatchList,
  isFinished,
  withCheckbox,
}) => {
  const toggleWatchList = useCallback(
    () =>
      toggleEpisodeInWatchList({
        id,
        name,
        episode,
        isFinished: false,
        isInWatchList: !isInWatchList,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [id]
  )

  const toggleCheckbox = useCallback(
    () =>
      toggleEpisodeWatchListStatus({
        id,
        isFinished,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isFinished]
  )

  const toggleWatchListDebounced = useDebounce(toggleWatchList, 300)
  const toggleCheckboxDebounced = useDebounce(toggleCheckbox, 300)

  const checkbox = (
    <StyledFormControlLabel
      label={!isFinished ? 'not seen' : 'viewed'}
      labelPlacement="start"
      control={
        <StyledCheckbox
          checked={isFinished}
          onChange={toggleCheckboxDebounced}
        />
      }
    />
  )

  return (
    <SearchListItem key={id}>
      <div>
        <h3>{name}</h3>
        <h5>{episode}</h5>
      </div>

      <ButtonBlock>
        {withCheckbox && checkbox}
        <Button
          text={isInWatchList ? '-' : '+'}
          background="transparent"
          color={!isInWatchList ? 'var(--bg-sucess)' : 'var(--bg-error)'}
          borderColor={!isInWatchList ? 'var(--bg-sucess)' : 'var(--bg-error)'}
          onClick={toggleWatchListDebounced}
        />
      </ButtonBlock>
    </SearchListItem>
  )
}

export default memo(EpisodeSearchListItem)
