import styled from 'styled-components'

import Checkbox from '@mui/material/Checkbox'

const StyledCheckbox = styled(Checkbox)(() => ({
  '& .MuiSvgIcon-root': {
    fontSize: 46,
    color: 'var(--bg-third)',
  },
  '& root': { backgroundColor: 'var(--bg-third)' },
}))

export default StyledCheckbox
