import styled from 'styled-components'

const ButtonBlock = styled.div`
  display: flex;
  align items: center;
  margin-bottom: 1rem;
  

  @media(min-width: 420px)
  {
    justify-content: space-between;
    margin-bottom: 0;
  }
`

export default ButtonBlock
