import styled from 'styled-components'
import { FormControlLabel } from '@mui/material'

const StyledFormControlLabel = styled(FormControlLabel)(() => ({
  marginRight: '1rem'
}))

export default StyledFormControlLabel