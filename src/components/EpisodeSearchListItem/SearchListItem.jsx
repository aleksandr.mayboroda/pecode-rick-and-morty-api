import styled from 'styled-components'

const SearchListItem = styled.li`
  display: flex;
  flex-direction: column;

  justify-content: space-between;
  flex-wrap: wrap;

  border-bottom: 1px solid var(--border-color-primary);

  @media (min-width: 420px) {
    flex-direction: row;
    align-items: center;
  }
`

export default SearchListItem
