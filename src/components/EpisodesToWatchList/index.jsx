import { memo } from 'react'

import { useReactiveVar } from '@apollo/client'
import { episodeWatchList } from '../../apollo/vars/episode'

import SearchList from '../Global/SearchList'
import EmptyData from '../Global/EmptyData'
import EpisodeSearchListItem from '../EpisodeSearchListItem'

const EpisodesToWatch = () => {
  const watchList = useReactiveVar(episodeWatchList)

  const list =
    !watchList.length > 0 ? (
      <EmptyData>List is empty</EmptyData>
    ) : (
      <SearchList>
        {watchList.map((props) => {
          return (
            <EpisodeSearchListItem
              key={props.id}
              {...props}
              withCheckbox={true}
            />
          )
        })}
      </SearchList>
    )

  return <>{list}</>
}

export default memo(EpisodesToWatch)
