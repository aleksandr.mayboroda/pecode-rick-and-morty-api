import styled from 'styled-components'

const ChatacterInfoModalList = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  max-height: 110px;
  overflow: auto;
  border-radius: var(----border-rad);
`

export default ChatacterInfoModalList