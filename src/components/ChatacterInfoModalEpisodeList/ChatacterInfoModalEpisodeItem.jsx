import styled from 'styled-components'

const ChatacterInfoModalEpisodeItem = styled.p`
  border: 1px solid var(--border-color-secondary);
  border-radius: var(--border-rad);
  margin: 0.25rem;
  padding: 0.5rem;
`

export default ChatacterInfoModalEpisodeItem