import { memo } from 'react'

import ChatacterInfoModalList from './ChatacterInfoModalList'
import ChatacterInfoModalEpisodeItem from './ChatacterInfoModalEpisodeItem'

const ChatacterInfoModalEpisodeList = ({ list }) => {
  const epicList = !list.length ? null : (
    <ChatacterInfoModalList>
      {list.map((epic) => (
        <ChatacterInfoModalEpisodeItem key={epic.id}>
          {epic.name}
        </ChatacterInfoModalEpisodeItem>
      ))}
    </ChatacterInfoModalList>
  )
  return <>{epicList}</>
}

export default memo(ChatacterInfoModalEpisodeList)
