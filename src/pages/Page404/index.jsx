import React from 'react'
import styled from 'styled-components'
import image from './page404.jpg'

import PageWrapper from '../../components/Global/PageWrapper'

const StyledPageWrapper = styled(PageWrapper)(() => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'flex-end',
  background: `url(${image}) no-repeat center`,
  backgroundSize: 'contained',
}))

const Wrapper = styled('div')(() => ({
  textAlign: 'center',
  height: '100%',

  // color: 'var(--color-text-primary)',
  fontWeight: 'bolder',
}))

const StyledHeader = styled('h1')`
  font-size: 4rem;
  background-color: var(--bg-secondary);
  padding: 1rem;

  animation: textToDown 2s linear both;
  animation-delay: 1;

  @keyframes textToDown {
    0% {
      margin-top: -500px;
    }
    50% {
      margin-top: -250px;
    }
    100% {
      margin-top: 0px;
    }
  }
`

const StyledSubHeader = styled('h2')`
  font-size: 2rem;
  text-transform: uppercase;
  background-color: var(--bg-secondary);
  padding: 0.5rem;
  animation: textToUp 2s linear both;
  animation-delay: 0;

  @keyframes textToUp {
    0% {
      margin-bottom: 500px;
    }
    50% {
      margin-bottom: 250px;
    }
    100% {
      margin-bottom: 50px;
    }
  }
`

const Page404 = () => {
  return (
    <StyledPageWrapper>
      <Wrapper>
        <StyledHeader>404</StyledHeader>
        <StyledSubHeader>page not found</StyledSubHeader>
      </Wrapper>
    </StyledPageWrapper>
  )
}

export default Page404
