import React from 'react'
import PageWrapper from '../../components/Global/PageWrapper'
import Episodes from '../../components/Episodes'

const EpisodesPage = () => {
  return (
    <PageWrapper>
      <Episodes />
    </PageWrapper>
  )
}

export default EpisodesPage