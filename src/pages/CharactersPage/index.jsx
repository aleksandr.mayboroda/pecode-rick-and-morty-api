import React from 'react'
import PageWrapper from '../../components/Global/PageWrapper'
import Characters from '../../components/Characters'

const CharactersPage = () => {
  return (
    <PageWrapper>
      <Characters />
    </PageWrapper>
  )
}

export default CharactersPage