import React from 'react'
import styled from 'styled-components'

import AppRoutes from './routes/AppRoutes'
import CustomLink from './components/Global/CustomLink'
import Modal from './components/Modal'

import Container from './components/Global/Container'

const StyledNav = styled('nav')((props) => ({
  backgroundColor: 'var(--bg-secondary)',
  padding: '2rem 1rem',
}))

const App = () => {
  return (
    <>
      <Container>
        <div style={{display: 'flex', flexDirection: 'column', minHeight: '100vh'}}>
          <StyledNav>
            <CustomLink to="character">Characters</CustomLink>
            <CustomLink to="episode">watch list</CustomLink>
          </StyledNav>
          <AppRoutes />
        </div>
      </Container>
      <Modal />
    </>
  )
}

export default App
