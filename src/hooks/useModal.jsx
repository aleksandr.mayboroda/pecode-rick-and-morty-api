import { useCallback } from 'react'
import { setModalContent } from '../apollo/vars/modal'

const useModal = () => {
  const modalOpen = useCallback((content) => setModalContent(content), [])
  const modalClose = useCallback(() => setModalContent(null), [])
  return { modalOpen, modalClose }
}

export default useModal
