import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import ErrorBoundary from './components/ErrorBoundary'

import { ApolloProvider } from '@apollo/client'
import apolloClient from './apollo/client'

import { BrowserRouter } from 'react-router-dom'
import './assets/style/params.css'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <ApolloProvider client={apolloClient}>
        <ErrorBoundary>
          <App />
        </ErrorBoundary>
      </ApolloProvider>
    </BrowserRouter>
  </React.StrictMode>
)
