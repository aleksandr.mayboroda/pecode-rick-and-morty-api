import React from 'react'
import { Navigate } from 'react-router-dom'

const Redirect = ({locateTo = '/'}) => {
  return <Navigate to={locateTo} />
}

export default Redirect