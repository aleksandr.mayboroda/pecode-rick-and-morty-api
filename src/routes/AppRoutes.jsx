import React from 'react'
import { Routes, Route } from 'react-router-dom'

import CharactersPage from '../pages/CharactersPage'
import EpisodesPage from '../pages/EpisodesPage'
import Page404 from '../pages/Page404'
import Redirect from './Redirect'

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Redirect locateTo={'/character'} />} />
      <Route path="character" element={<CharactersPage />} />
      <Route path="episode" element={<EpisodesPage />} />
      <Route path="*" element={<Page404 />} />
    </Routes>
  )
}

export default AppRoutes
