import { gql } from '@apollo/client'

export const GET_CHARACTERS = gql`
  query AllCharacters($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        status
        species
        gender
        created
        image
        episode {
          id
          name
        }
        location {
          id
          name
        }
        origin {
          id
          name
        }
      }
    }
  }
`
