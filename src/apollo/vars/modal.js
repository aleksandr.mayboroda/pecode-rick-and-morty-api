import { makeVar } from '@apollo/client'

export const modalContent = makeVar(null)
export const setModalContent = (newContent) => modalContent(newContent)