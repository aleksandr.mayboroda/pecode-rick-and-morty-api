import { makeVar } from '@apollo/client'

export const episodeListPage = makeVar(1)
export const setEpisodeListPage = (newPage) => episodeListPage(newPage)

const initialFilters = {
  name: '',
}

const initialEpisodeWatchList =
  JSON.parse(localStorage.getItem('episodeWatchList')) || []

export const episodeFilters = makeVar({ ...initialFilters })
export const setEpisodeFilter = (name, value) =>
  episodeFilters({ ...episodeFilters(), [name]: value })

export const episodeWatchList = makeVar([...initialEpisodeWatchList])

export const isEpisodeInWatchList = (id) =>
  !!episodeWatchList().find((el) => el.id === id)

export const toggleEpisodeInWatchList = (episode) => {
  const isInList = !!episodeWatchList().find((el) => el.id === episode.id)

  const newWatchList = !isInList
    ? [...episodeWatchList(), episode]
    : episodeWatchList().filter((item) => item.id !== episode.id)

  localStorage.setItem('episodeWatchList', JSON.stringify(newWatchList))
  episodeWatchList(newWatchList)
}

export const toggleEpisodeWatchListStatus = (episode) => {
  const newWatchList = episodeWatchList().map((item) =>
    item.id !== episode.id ? item : { ...item, isFinished: !episode.isFinished }
  )
 
  localStorage.setItem('episodeWatchList', JSON.stringify(newWatchList))
  episodeWatchList(newWatchList)
}
