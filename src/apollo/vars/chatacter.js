import { makeVar } from '@apollo/client'

export const charListPage = makeVar(1)
export const setCharListPage = (newPage) => charListPage(newPage)



const initialFilters = {
  species: '',
  gender: '',
  status: '',
}

export const charFilters = makeVar({...initialFilters})
export const setCharFilter = (name, value) => charFilters({...charFilters(), [name]: value})