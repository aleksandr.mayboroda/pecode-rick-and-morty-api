import { InMemoryCache } from '@apollo/client'
import { isEpisodeInWatchList } from './vars/episode'

const cache = new InMemoryCache({
  typePolicies: {
    Episode: {
      fields: {
        isInWatchList: {
          read(_, { readField }) {
            const id = readField('id')
            return isEpisodeInWatchList(id)
          },
        },
      },
    },
  },
})

export default cache
